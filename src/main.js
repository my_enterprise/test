import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import router from './router';
import { Tabbar, TabbarItem } from 'vant';

import 'amfe-flexible';

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(Tabbar)
Vue.use(TabbarItem)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')