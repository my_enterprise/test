import Home from '../components/Home'
import Address from '../components/Address'
import Invite from '../components/Invite'
export default [
  {
    path: '/',
    component: Home
  },
  {
    path: '/address',
    component: Address
  },
  {
    path: '/invite',
    component: Invite
  }
]